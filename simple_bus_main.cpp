/*****************************************************************************

  The following code is derived, directly or indirectly, from the SystemC
  source code Copyright (c) 1996-2014 by all Contributors.
  All Rights reserved.

  The contents of this file are subject to the restrictions and limitations
  set forth in the SystemC Open Source License (the "License");
  You may not use this file except in compliance with such restrictions and
  limitations. You may obtain instructions on how to receive a copy of the
  License at http://www.accellera.org/. Software distributed by Contributors
  under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
  ANY KIND, either express or implied. See the License for the specific
  language governing rights and limitations under the License.

 *****************************************************************************/

/*****************************************************************************
 
  simple_bus_main.cpp : sc_main
 
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
 
 *****************************************************************************/
 
/*****************************************************************************
 
  MODIFICATION LOG - modifiers, enter your name, affiliation, date and
  changes you are making here.
 
      Name, Affiliation, Date:
  Description of Modification:
 
 *****************************************************************************/
#include <stdio.h>
#include <string.h>
#include "systemc.h"
#include "simple_bus_test.h"

#define N_SCENARIOS 18

unsigned int master_blocking_1_priorities[N_SCENARIOS] 
  = {5,5,5,5,5,5,5,5,5,3,3,3,3,3,3,3,3,3};
unsigned int master_blocking_2_priorities[N_SCENARIOS] 
  = {6,6,6,6,6,6,6,6,6,4,4,4,4,4,4,4,4,4};
unsigned int master_non_blocking_1_priorities[N_SCENARIOS] 
  = {3,3,3,3,3,3,3,3,3,5,5,5,5,5,5,5,5,5};
unsigned int master_non_blocking_2_priorities[N_SCENARIOS] 
  = {4,4,4,4,4,4,4,4,4,6,6,6,6,6,6,6,6,6};
unsigned int master_blocking_timeout[N_SCENARIOS] 
  = {10,10,10,20,20,20,50,50,50,10,10,10,20,20,20,50,50,50};
unsigned int master_non_blocking_timeout[N_SCENARIOS] 
  = {10,10,10,20,20,20,50,50,50,10,10,10,20,20,20,50,50,50};
unsigned int burst_length[N_SCENARIOS] 
  = {10,20,50,10,20,50,10,20,50,10,20,50,10,20,50,10,20,50};

int simulation_time = 10000;

float blocking_master_utilization_level[N_SCENARIOS];

float non_blocking_master_utilization_level[N_SCENARIOS] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,};
float direct_master_utilization_level[N_SCENARIOS] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,};

float blocking_master_average_latency[N_SCENARIOS] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,};
float non_blocking_master_average_latency[N_SCENARIOS] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,};
float direct_master_average_latency[N_SCENARIOS] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,};

float globalUtilizationLevel[N_SCENARIOS] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,};

float troughput[N_SCENARIOS] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,};

float get_utilization_percentage(int cycles_used, int simulation_cycles){
  return float(cycles_used)/float(simulation_cycles)*100;
}

float get_global_utilization_level(int bus_not_required_cycles, int bus_wait_cycles, int bus_error_cycles, int simulation_time){
  return float(simulation_time 
    - (bus_not_required_cycles + bus_wait_cycles + bus_error_cycles))
    /simulation_time;
}

float get_average_latency(int cycles_used, int number_of_operations){
  return float(cycles_used)/float(number_of_operations);
}

void run_one_scenario(int index){
  simple_bus_test *top = new simple_bus_test("top",
                      master_blocking_1_priorities[index],
                      master_blocking_2_priorities[index],
                      master_non_blocking_1_priorities[index],
                      master_non_blocking_2_priorities[index],
                      master_blocking_timeout[index],
                      master_blocking_timeout[index],
                      master_non_blocking_timeout[index],
                      master_non_blocking_timeout[index],
                      burst_length[index]);



  /*
  unsigned int priority_master_b1,
    unsigned int priority_master_b2,
    unsigned int priority_master_nb1,
    unsigned int priority_master_nb2,
    unsigned int timeout_master_b1,
    unsigned int timeout_master_b2,
    unsigned int timeout_master_nb1,
    unsigned int timeout_master_nb2,
    unsigned int burst_length)
      */

  sc_start(simulation_time, SC_NS);
  sc_stop();

  int bus_not_required_cycles = ((top->bus)->noRequired);
  int bus_wait_cycles = ((top->bus)->busWait);
  int bus_error_cycles = ((top->bus)->busError);
  int busOK= ((top->bus)->busOK);

  blocking_master_utilization_level[index] = get_utilization_percentage((top->bus)->masters_time_of_rw_operations[0], simulation_time);
  non_blocking_master_utilization_level[index] = get_utilization_percentage((top->bus)->masters_time_of_rw_operations[1], simulation_time);
  direct_master_utilization_level[index] = get_utilization_percentage((top->bus)->masters_time_of_use[2], simulation_time);

  float blocking_master_number_of_operations = (top->bus)->masters_number_of_operations[0];
  float non_blocking_master_number_of_operations = (top->bus)->masters_number_of_operations[1];
  float direct_master_number_of_operations = (top->bus)->masters_time_of_use[2];

  blocking_master_average_latency[index] = get_average_latency((top->bus)->masters_time_of_use[0], blocking_master_number_of_operations)/burst_length[index];
  non_blocking_master_average_latency[index] = get_average_latency((top->bus)->masters_time_of_use[1], non_blocking_master_number_of_operations);
  direct_master_average_latency[index] = get_average_latency((top->bus)->masters_time_of_use[2],direct_master_number_of_operations);

  globalUtilizationLevel[index] = get_global_utilization_level(bus_not_required_cycles, bus_wait_cycles, bus_error_cycles, simulation_time);

  troughput[index] = float(busOK)/simulation_time*4;

  delete top;
}

void show_vector(float vector[N_SCENARIOS]){
  
  for(int i = 0; i < N_SCENARIOS; i++)
    sb_fprintf(stdout,"%f, ",vector[i]);

  sb_fprintf(stdout,"\n");
}

void show_output(){

  sb_fprintf(stdout,"                                  ");

  for (float i = 1; i <= N_SCENARIOS; i++)
    sb_fprintf(stdout,"%f, ", i);    

  sb_fprintf(stdout,"\n\n");

  sb_fprintf(stdout,"Blocking master utilization:      ");
  show_vector(blocking_master_utilization_level);

  sb_fprintf(stdout,"Non-blocking master utilization:  ");
  show_vector(non_blocking_master_utilization_level);

  sb_fprintf(stdout,"Blocking master avg. latency:     ");
  show_vector(blocking_master_average_latency);

  sb_fprintf(stdout,"Non blocking master avg. latency: ");
  show_vector(non_blocking_master_average_latency);

  sb_fprintf(stdout,"Global utilization level:         ");
  show_vector(globalUtilizationLevel);

  sb_fprintf(stdout,"Throughput                        ");
  show_vector(troughput);
}

void init(){
}

int sc_main(int p, char **params)
{

  init();

  sb_fprintf(stdout,"param: %s ", params[1]);

  int parsed_value = atoi(params[1]);

  sb_fprintf(stdout,"parsed: %i: ",  parsed_value);

  run_one_scenario(parsed_value);
  
  
  show_output();
  /*
  simple_bus_test top("top",
                      5,
                      6,
                      3,
                      4,
                      10,
                      10,
                      10,
                      10,
                      10);
*/


  /*
  unsigned int priority_master_b1,
    unsigned int priority_master_b2,
    unsigned int priority_master_nb1,
    unsigned int priority_master_nb2,
    unsigned int timeout_master_b1,
    unsigned int timeout_master_b2,
    unsigned int timeout_master_nb1,
    unsigned int timeout_master_nb2,
    unsigned int burst_length)
      */
/*
  sc_start(simulation_time, SC_NS);

  int bus_not_required_cycles = ((top.bus)->noRequired);
  int bus_wait_cycles = ((top.bus)->busWait);
  int bus_error_cycles = ((top.bus)->busError);
  int busOK= ((top.bus)->busOK);

  float blocking_master_utilization_level = get_utilization_percentage((top.bus)->masters_time_of_rw_operations[0], simulation_time);
  float non_blocking_master_utilization_level = get_utilization_percentage((top.bus)->masters_time_of_rw_operations[1], simulation_time);
  float direct_master_utilization_level = get_utilization_percentage((top.bus)->masters_time_of_use[2], simulation_time);

  float blocking_master_number_of_operations = (top.bus)->masters_number_of_operations[0];
  float non_blocking_master_number_of_operations = (top.bus)->masters_number_of_operations[1];
  float direct_master_number_of_operations = (top.bus)->masters_time_of_use[2];

  float blocking_master_average_latency = get_average_latency((top.bus)->masters_time_of_use[0], blocking_master_number_of_operations);
  float non_blocking_master_average_latency = get_average_latency((top.bus)->masters_time_of_use[1], non_blocking_master_number_of_operations);
  float direct_master_average_latency = get_average_latency((top.bus)->masters_time_of_use[2],direct_master_number_of_operations);

  float globalUtilizationLevel = get_global_utilization_level(bus_not_required_cycles, bus_wait_cycles, bus_error_cycles, simulation_time);

  float troughput = float(busOK)/simulation_time;
*/
/*
  sb_fprintf(stdout,"\n SIMULATION TIME: %i cycles (1 cycle = 1 ns) \n",simulation_time);
  sb_fprintf(stdout," Bus not required: %i cycles \n", bus_not_required_cycles);
  sb_fprintf(stdout," Bus wait: %i cycles \n", bus_wait_cycles);
  sb_fprintf(stdout," Bus error: %i cycles \n\n", bus_error_cycles);

  sb_fprintf(stdout," Utilization Level (global): %f \% \n",globalUtilizationLevel*100);
  sb_fprintf(stdout," Utilization level (blocking) : %f \% \n", blocking_master_utilization_level);
  sb_fprintf(stdout," Utilization level (non blocking) %f \%\n", non_blocking_master_utilization_level);
  sb_fprintf(stdout," Utilization level (direct) 0 \%\n\n");

  sb_fprintf(stdout," Troughput: %f bytes/cycle \n",troughput*4);
  sb_fprintf(stdout," Troughput: %f Gigabytes/s \n\n",troughput*4);
  
  sb_fprintf(stdout," Average latency (blocking): %f cycles \n", blocking_master_average_latency/16);
  sb_fprintf(stdout," Average latency (non blocking): %f cycles \n", non_blocking_master_average_latency);
  sb_fprintf(stdout," Average latency (direct): 0 cycles \n\n");

  sb_fprintf(stdout," Number of operations (blocking): %f \n", blocking_master_number_of_operations);
  sb_fprintf(stdout," Number of operations (non blocking): %f \n", non_blocking_master_number_of_operations);
  sb_fprintf(stdout," Number of operations (direct): %f \n\n", direct_master_number_of_operations/4);

  sb_fprintf(stdout," Cycles used (blocking): %i \n", (top.bus)->masters_time_of_use[0]);
  sb_fprintf(stdout," Cycles used (non blocking): %i \n", (top.bus)->masters_time_of_use[1]);
  sb_fprintf(stdout," Cycles used (direct): %i \n\n", (top.bus)->masters_time_of_use[2]/4);
  
  sb_fprintf(stdout," My global: %i \n\n", my_global);
*/
  return 0;
}
