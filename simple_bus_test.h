/*****************************************************************************

  The following code is derived, directly or indirectly, from the SystemC
  source code Copyright (c) 1996-2014 by all Contributors.
  All Rights reserved.

  The contents of this file are subject to the restrictions and limitations
  set forth in the SystemC Open Source License (the "License");
  You may not use this file except in compliance with such restrictions and
  limitations. You may obtain instructions on how to receive a copy of the
  License at http://www.accellera.org/. Software distributed by Contributors
  under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
  ANY KIND, either express or implied. See the License for the specific
  language governing rights and limitations under the License.

 *****************************************************************************/

/*****************************************************************************
 
  simple_bus_test.h : The test bench.
 
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
 
 *****************************************************************************/
 
/*****************************************************************************
 
  MODIFICATION LOG - modifiers, enter your name, affiliation, date and
  changes you are making here.
 
      Name, Affiliation, Date:
  Description of Modification:
 
 *****************************************************************************/

#ifndef __simple_bus_test_h
#define __simple_bus_test_h

#include <systemc.h>

#include "simple_bus_master_blocking.h"
#include "simple_bus_master_non_blocking.h"
#include "simple_bus_master_direct.h"
#include "simple_bus_slow_mem.h"
#include "simple_bus.h"
#include "simple_bus_fast_mem.h"
#include "simple_bus_arbiter.h"
#include "simple_bus_test.h"

unsigned int my_global = 1;

SC_MODULE(simple_bus_test)
{
  // channels
  sc_clock C1;

  SC_HAS_PROCESS(simple_bus_test);

  // module instances
  simple_bus_master_blocking     *master_b1;
  simple_bus_master_blocking     *master_b2;
  simple_bus_master_non_blocking *master_nb1;
  simple_bus_master_non_blocking *master_nb2;
  simple_bus_master_direct       *master_d;
  simple_bus_slow_mem            *mem_slow;
  simple_bus                     *bus;
  simple_bus_fast_mem            *mem_fast;
  simple_bus_arbiter             *arbiter;

/*
simple_bus_master_blocking(sc_module_name name_
           , unsigned int unique_priority
           , unsigned int address
                             , bool lock
                             , int timeout)
    : sc_module(name_)
    , m_unique_priority(unique_priority)
    , m_address(address)
    , m_lock(lock)
    , m_timeout(timeout)
*/
  // constructor
  
  /*
  SC_CTOR(simple_bus_test,
    unsigned int priority_master_b1,
    unsigned int priority_master_b2,
    unsigned int priority_master_nb1,
    unsigned int priority_master_nb2,
    unsigned int timeout_master_b1,
    unsigned int timeout_master_b2,
    unsigned int timeout_master_nb1,
    unsigned int timeout_master_nb2,
    unsigned int burst_length)
    : C1("C1")*/

  simple_bus_test(sc_module_name name_,
    unsigned int priority_master_b1,
    unsigned int priority_master_b2,
    unsigned int priority_master_nb1,
    unsigned int priority_master_nb2,
    unsigned int timeout_master_b1,
    unsigned int timeout_master_b2,
    unsigned int timeout_master_nb1,
    unsigned int timeout_master_nb2,
    unsigned int burst_length)
    :sc_module(name_)
    ,m_priority_master_b1(priority_master_b1)
    ,m_priority_master_b2(priority_master_b2)
    ,m_priority_master_nb1(priority_master_nb1)
    ,m_priority_master_nb2(priority_master_nb2)
    ,m_timeout_master_b1(timeout_master_b1)
    ,m_timeout_master_b2(timeout_master_b2)
    ,m_timeout_master_nb1(timeout_master_nb1)
    ,m_timeout_master_nb2(timeout_master_nb2)
    ,m_burst_length(burst_length)
  {

    //bus = new simple_bus("bus");

    master_b1 = new simple_bus_master_blocking("master_b1", m_priority_master_b1, 0x4c, false, m_timeout_master_b1, m_burst_length); 
    master_b2= new simple_bus_master_blocking("master_b2", m_priority_master_b2, 0x5c, false, m_timeout_master_b2, m_burst_length); 
    master_nb1 = new simple_bus_master_non_blocking("master_nb1", m_priority_master_nb1, 0x28, false, m_timeout_master_nb1); 
    master_nb2 = new simple_bus_master_non_blocking("master_nb2", m_priority_master_nb2, 0x38, false, m_timeout_master_nb2);

    master_d = new simple_bus_master_direct("master_d", 0x78, 100);
    mem_fast = new simple_bus_fast_mem("mem_fast", 0x00, 0x7f);
    mem_slow = new simple_bus_slow_mem("mem_slow", 0x80, 0xff, 1);
     bus = new simple_bus("bus",false); // verbose output
    
    // arbiter = new simple_bus_arbiter("arbiter",true); // verbose output
    arbiter = new simple_bus_arbiter("arbiter");

    arbiter->clock(C1);
    master_d->clock(C1);
    bus->clock(C1);
    master_b1->clock(C1);
    master_b2->clock(C1);
    master_nb1->clock(C1);
    master_nb2->clock(C1);
    mem_slow->clock(C1);
    master_d->bus_port(*bus);
    master_b1->bus_port(*bus);
    master_b2->bus_port(*bus);
    master_nb1->bus_port(*bus);
    master_nb2->bus_port(*bus);
    bus->arbiter_port(*arbiter);
    bus->slave_port(*mem_slow);
    bus->slave_port(*mem_fast);

    bus->priority_master_b1 = m_priority_master_b1;
    bus->priority_master_b2 = m_priority_master_b2;
    bus->priority_master_nb1 = m_priority_master_nb1;
    bus->priority_master_nb2 = m_priority_master_nb2;
    bus->timeout_master_b1 = m_timeout_master_b1;
    bus->timeout_master_b2 = m_timeout_master_b2;
    bus->timeout_master_nb1 = m_timeout_master_nb1;
    bus->timeout_master_nb2 = m_timeout_master_nb2;
  }

  ~simple_bus_test()
  {
    if (master_b1) {delete master_b1; master_b1 = 0;}
    if (master_b2) {delete master_b2; master_b2 = 0;}
    if (master_nb1) {delete master_nb1; master_nb1 = 0;}
    if (master_nb2) {delete master_nb2; master_nb2 = 0;}
    if (master_d) {delete master_d; master_d = 0;}
    if (mem_slow) {delete mem_slow; mem_slow = 0;}
    if (bus) {delete bus; bus = 0;}
    if (mem_fast) {delete mem_fast; mem_fast = 0;}
    if (arbiter) {delete arbiter; arbiter = 0;}
  }

  private:
    unsigned int m_priority_master_b1;
    unsigned int m_priority_master_b2;
    unsigned int m_priority_master_nb1;
    unsigned int m_priority_master_nb2;
    unsigned int m_timeout_master_b1;
    unsigned int m_timeout_master_b2;
    unsigned int m_timeout_master_nb1;
    unsigned int m_timeout_master_nb2;
    unsigned int m_burst_length;

}; // end class simple_bus_test

#endif
